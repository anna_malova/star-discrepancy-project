import optimization.HaltonGenerator;
import optimization.HaltonGeneratorFactory;
import optimization.crossovers.HaltonGeneratorCrossoverPMX;
import optimization.evaluators.HaltonGeneratorDemEvaluator;
import optimization.mutations.HaltonGeneratorThrorsMutation;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.StochasticUniversalSampling;
import org.uncommons.watchmaker.framework.termination.ElapsedTime;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.min;

public class Main implements Runnable {
    public static HaltonGenerator evolveHaltonGenerator(int dim, int[] primes, int n) {
        List<EvolutionaryOperator<HaltonGenerator>> operators = new ArrayList<>();
        //operators.add(new HaltonGeneratorCrossoverOX(1));
        operators.add(new HaltonGeneratorCrossoverPMX(1));
        //operators.add(new HaltonGeneratorCenterInverseMutation((new Probability(0.7d))));
        //operators.add(new HaltonGeneratorPartialShuffleMutation(new Probability(0.7d)));
        //operators.add(new HaltonGeneratorInverseSequenceMutation(new Probability(0.7d)));
        operators.add(new HaltonGeneratorThrorsMutation(new Probability(0.7d)));
        //operators.add(new HaltonGeneratorTworsMutation(new Probability(0.7d)));
        EvolutionaryOperator<HaltonGenerator> pipeline = new EvolutionPipeline<>(operators);
        GenerationalEvolutionEngine<HaltonGenerator> engine = new GenerationalEvolutionEngine<HaltonGenerator>(
                new HaltonGeneratorFactory(dim, primes),
                pipeline,
                new HaltonGeneratorDemEvaluator(n),
                new StochasticUniversalSampling(),
                new MersenneTwisterRNG()
        );
        engine.setSingleThreaded(true);
        engine.addEvolutionObserver(data -> {
            int tmp = data.getGenerationNumber();
            if (tmp % 10 == 0) {
                System.err.println("Generation # " + data.getGenerationNumber());
            }
        });
        return engine.evolve(50, 2, new ElapsedTime(2 * 60 * 1000));
    }

    public static double performRandomSearch(int dim, int[] primes, int n) {
        double thebest = Double.MAX_VALUE;
        for (int i = 0; i < 20; i++) {
//            if (i % 10 == 0) {
//                System.err.println(i);
//            }
            HaltonGenerator hg = new HaltonGenerator(dim, primes);
            thebest = min(thebest, hg.getDemDiscr(n));
        }
        return thebest;
    }

    public void pipeline() {
        int dim = 4;
        int[] primes = {41, 41, 41, 41, 41, 41};
        for (int n = 4; n <= 1024; n *= 2) {
            HaltonGenerator hg = evolveHaltonGenerator(dim, primes, n);
            System.out.println(hg.getDemDiscr(n));
            //System.out.println(performRandomSearch(dim, primes, n));
        }
    }


    @Override
    public void run() {
        pipeline();
    }

    public static void main(String[] args) {
        new Main().run();
    }
}
