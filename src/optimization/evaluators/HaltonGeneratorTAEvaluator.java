package optimization.evaluators;

import optimization.HaltonGenerator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.util.List;

public class HaltonGeneratorTAEvaluator implements FitnessEvaluator<HaltonGenerator> {
    public HaltonGeneratorTAEvaluator(int n) {
        this.n = n;
    }

    @Override
    public double getFitness(HaltonGenerator hg, List<? extends HaltonGenerator> list) {
        return hg.getTADiscr(n);
    }

    @Override
    public boolean isNatural() {
        return false;
    }
    protected int n;
}