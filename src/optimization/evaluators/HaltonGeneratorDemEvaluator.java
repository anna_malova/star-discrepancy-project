package optimization.evaluators;

import optimization.HaltonGenerator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.util.List;

public class HaltonGeneratorDemEvaluator implements FitnessEvaluator<HaltonGenerator> {
    public HaltonGeneratorDemEvaluator(int n) {
        this.n = n;
    }

    @Override
    public double getFitness(HaltonGenerator hg, List<? extends HaltonGenerator> list) {
        return hg.getDemDiscr(n);
    }

    @Override
    public boolean isNatural() {
        return false;
    }
    protected int n;
}
