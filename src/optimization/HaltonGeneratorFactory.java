package optimization;

import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.Random;

// This class allows to generate random Halton Generator
public class HaltonGeneratorFactory extends AbstractCandidateFactory<HaltonGenerator> {
    public HaltonGeneratorFactory(int dim, int[] primes) {
        this.dim = dim;
        this.primes = primes;
    }

    @Override
    public HaltonGenerator generateRandomCandidate(Random random) {
        return new HaltonGenerator(dim, primes, random);
    }

    private int dim;
    private int[] primes;
}
