package optimization;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.DoubleByReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

public class HaltonGenerator {
    public HaltonGenerator(int dimension, int[] primes, Random rnd) {
        this.dimension = dimension;
        this.primes = primes;
        this.decomposition = new ArrayList<>();
        for (int i = 0; i < dimension; i++) {
            decomposition.add(new ArrayList<>());
        }
        this.points = new ArrayList<>();
        this.rnd = rnd;
        this.initialize(primes);
    }

    public HaltonGenerator(int dimension, int[] primes) {
        this(dimension, primes, new Random());
    }

    public HaltonGenerator(int dimension, int[] primes, ArrayList<ArrayList<Integer>> permutations) {
        this.dimension = dimension;
        this.primes = primes;
        this.decomposition = new ArrayList<>();
        for (int i = 0; i < dimension; i++) {
            decomposition.add(new ArrayList<>());
        }
        this.points = new ArrayList<>();
        this.rnd = new Random();
        this.permutations = permutations;
    }

    public void initialize(int[] primes) {
        if (primes.length < dimension) {
            throw new IllegalArgumentException("Number of primes should be more or equal dimension");
        }

        this.primes = primes;
        permutations = new ArrayList<>();
        for (int i = 0; i < dimension; i++) {
            permutations.add(new ArrayList<>());
            for (int j = 0; j < primes[i]; j++) {
                permutations.get(i).add(j);
            }

            int size = permutations.get(i).size();
            for (int j = size - 1; j > 1; j--)
                Collections.swap(permutations.get(i), j, rnd.nextInt(j) + 1);
        }
    }

//    public void initialize(int prime) {
//        int[] primes = IntStream.iterate(0, x -> prime).limit(dimension).toArray();
//        initialize(primes);
//    }
//
//    // Create permutations for each dimension
//    public void initialize() {
//        int n = Constants.PRIMES.length;
//        int prime = Constants.PRIMES[rnd.nextInt(n)];
//        initialize(prime);
//    }

//    public boolean isSameGenerator(optimization.HaltonGenerator another) {
//        return Arrays.equals(primes, another.primes);
//    }

    public static boolean isPermutation(ArrayList<Integer> a) {
        int n = a.size();
        HashSet<Integer> hs = new HashSet(2 * n);
        a.forEach(x -> hs.add(x));
        for (int i = 0; i < n; i++) {
            if (!hs.contains(i)) {
                System.out.println("Panic-panic");
                return false;
            }
        }
        return true;
    }

    // generate first num points
    public void generate(int num) {
        for (int i = points.size(); i < num; i++) {
            points.add(new ArrayList<>());

            for (int p = 0; p < dimension; p++) {
                if (permutations.get(p).get(0) != 0) {
                    System.err.println("Panic!");
                }
                int index = 0;
                while ((index < decomposition.get(p).size()) && (decomposition.get(p).get(index) == (primes[p] - 1))) {
                    decomposition.get(p).set(index, 0);
                    index++;
                }

                if(index == decomposition.get(p).size()) {
                    decomposition.get(p).add(1);
                } else {
                    int tmp = decomposition.get(p).get(index) + 1;
                    decomposition.get(p).set(index, tmp);
                }

                int decompositionSize = decomposition.get(p).size();
                long base = primes[p];

                int lastIndex = decomposition.get(p).get(decompositionSize - 1);
                double currentValue = permutations.get(p).get(lastIndex);
                for (int k = decompositionSize - 1; k >= 1; k--) {
                    currentValue += (permutations.get(p).get(decomposition.get(p).get(k-1)) * base);
                    base *= primes[p];
                }
                if (currentValue / base > 1) {
                    System.err.println("PANIC!!!!");
                }
                points.get(i).add(currentValue / base);
            }
        }
    }

    // calculate discrepancy with DEM algorithm for first n points
    public double getDemDiscr(int n) {
        if (points.size() < n) {
            generate(n);
        }

        CLibrary clib = (CLibrary) Native.loadLibrary("discr", CLibrary.class);

        Pointer pointset = new Memory(n * PTR_SIZE);
        for (int i = 0; i < n; i++) {
            Pointer pointset2 = new Memory(DOUBLE_SIZE * dimension);
            for (int j = 0; j < dimension; j++) {
                pointset2.setDouble(j * DOUBLE_SIZE, points.get(i).get(j));
            }
            pointset.setPointer(i * PTR_SIZE, pointset2);
        }

        DoubleByReference lower = new DoubleByReference();

        // return clib.oydiscr(pointset, dimension, n, lower);
        return clib.oydiscr(pointset, dimension, n, lower);
    }

    public double getTADiscr(int n) {
        if (points.size() < n) {
            generate(n);
        }

        CLibrary clib = (CLibrary) Native.loadLibrary("tadiscr", CLibrary.class);

        Pointer pointset = new Memory(n * PTR_SIZE);
        for (int i = 0; i < n; i++) {
            Pointer pointset2 = new Memory(DOUBLE_SIZE * dimension);
            for (int j = 0; j < dimension; j++) {
                pointset2.setDouble(j * DOUBLE_SIZE, points.get(i).get(j));
            }
            pointset.setPointer(i * PTR_SIZE, pointset2);
        }

        // return clib.oydiscr(pointset, dimension, n, lower);
        double x = clib.oldmain(pointset, n, dimension);
        return clib.oldmain(pointset, n, dimension);
    }

    public HaltonGenerator clone() {
        HaltonGenerator hg = new HaltonGenerator(dimension, primes);
        hg.permutations = (ArrayList<ArrayList<Integer>>) permutations.clone();
        return hg;
    }

    public ArrayList<ArrayList<Double>> points;
    private ArrayList<ArrayList<Integer>> decomposition;
    public ArrayList<ArrayList<Integer>> permutations;

    private int DOUBLE_SIZE = Native.getNativeSize(Double.TYPE);
    private int PTR_SIZE = Native.getNativeSize(Pointer.class);

    private Random rnd;
    public final int dimension;
    public int[] primes;
}
