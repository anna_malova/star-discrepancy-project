package optimization.mutations;

import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.Probability;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class HaltonGeneratorThrorsMutation extends HaltonGeneratorMutation {
    public HaltonGeneratorThrorsMutation(NumberGenerator<Probability> mutationProbability, NumberGenerator<Integer> mutationCount) {
        super(mutationProbability, mutationCount);
    }

    public HaltonGeneratorThrorsMutation(Probability mutationProbability) {
        super(mutationProbability);
    }

    @Override
    protected ArrayList<Integer> mutate(ArrayList<Integer> parent, Random rnd) {
        //HaltonGenerator.isPermutation(parent);
        int n = parent.size();
        ArrayList<Integer> child = new ArrayList<>(parent);
        int[] indexes = {1 + rnd.nextInt(n - 1), 1 + rnd.nextInt(n - 1), 1 + rnd.nextInt(n - 1)};
        while (indexes[0] == indexes[1]) {
            indexes[1] = 1 + rnd.nextInt(n - 1);
        }
        while ((indexes[0] == indexes[2]) || (indexes[1] == indexes[2])) {
            indexes[2] = 1 + rnd.nextInt(n - 1);
        }

        Arrays.sort(indexes);
        child.set(indexes[0], parent.get(indexes[2]));
        child.set(indexes[1], parent.get(indexes[0]));
        child.set(indexes[2], parent.get(indexes[1]));

        return child;
    }
}
