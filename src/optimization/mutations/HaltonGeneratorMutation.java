package optimization.mutations;

import optimization.HaltonGenerator;
import org.uncommons.maths.number.ConstantGenerator;
import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class HaltonGeneratorMutation implements EvolutionaryOperator<HaltonGenerator> {
    public HaltonGeneratorMutation(NumberGenerator<Probability> mutationProbability, NumberGenerator<Integer> mutationCount) {
        this.mutationProbability = mutationProbability;
        this.mutationCount = mutationCount;
    }

    public HaltonGeneratorMutation(Probability mutationProbability) {
        this(new ConstantGenerator<>(mutationProbability), new ConstantGenerator<>(1));
    }

    protected abstract ArrayList<Integer> mutate(ArrayList<Integer> parent, Random rnd);

    protected HaltonGenerator mutate(HaltonGenerator parent, Random rnd) {
        if (mutationProbability.nextValue().nextEvent(rnd)) {
            ArrayList<ArrayList<Integer>> childPermutation = new ArrayList<>();
            for (ArrayList<Integer> permutation : parent.permutations) {
                childPermutation.add(mutate(permutation, rnd));
            }
            return new HaltonGenerator(parent.dimension, parent.primes, childPermutation);
        }
        return parent.clone();
    }

    @Override
    public List<HaltonGenerator> apply(List<HaltonGenerator> selectedCandidates, Random rnd) {
        List<HaltonGenerator> mutatedPopulation = new ArrayList<>();
        selectedCandidates.forEach(candidate -> mutatedPopulation.add(mutate(candidate, rnd)));

        return mutatedPopulation;
    }

    protected final NumberGenerator<Probability> mutationProbability;
    protected final NumberGenerator<Integer> mutationCount;
}
