package optimization.mutations;

import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.Probability;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class HaltonGeneratorPartialShuffleMutation extends HaltonGeneratorMutation {
    public HaltonGeneratorPartialShuffleMutation(NumberGenerator<Probability> mutationProbability, NumberGenerator<Integer> mutationCount) {
        super(mutationProbability, mutationCount);
    }

    public HaltonGeneratorPartialShuffleMutation(Probability mutationProbability) {
        super(mutationProbability);
    }

    @Override
    protected ArrayList<Integer> mutate(ArrayList<Integer> parent, Random rnd) {
        ArrayList<Integer> child = new ArrayList<>(parent);
        int n = parent.size();
        int index = rnd.nextInt(n);
        for (int i = index - 1; i > 1; i--)
            Collections.swap(child, i, rnd.nextInt(i) + 1);

        return child;
    }
}
