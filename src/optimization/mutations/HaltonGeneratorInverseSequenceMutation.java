package optimization.mutations;

import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.Probability;

import java.util.ArrayList;
import java.util.Random;

public class HaltonGeneratorInverseSequenceMutation extends HaltonGeneratorMutation{
    public HaltonGeneratorInverseSequenceMutation(NumberGenerator<Probability> mutationProbability, NumberGenerator<Integer> mutationCount) {
        super(mutationProbability, mutationCount);
    }

    public HaltonGeneratorInverseSequenceMutation(Probability mutationProbability) {
        super(mutationProbability);
    }

    @Override
    protected ArrayList<Integer> mutate(ArrayList<Integer> parent, Random rnd) {
        int n = parent.size();
        ArrayList<Integer> child = new ArrayList<>(parent);
        int start = 1 + rnd.nextInt(n - 1); int end = 1 + rnd.nextInt(n - 1);
        if (start > end) {
            int tmp = start;
            start = end;
            end = tmp;
        }
        for (int i = 0; i <= end - start; i++) {
            child.set(start + i, parent.get(end - i));
        }

        return child;
    }
}
