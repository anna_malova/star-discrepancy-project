package optimization.mutations;

import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.Probability;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class HaltonGeneratorTworsMutation extends HaltonGeneratorMutation{
    public HaltonGeneratorTworsMutation(NumberGenerator<Probability> mutationProbability, NumberGenerator<Integer> mutationCount) {
        super(mutationProbability, mutationCount);
    }

    public HaltonGeneratorTworsMutation(Probability mutationProbability) {
        super(mutationProbability);
    }

    @Override
    protected ArrayList<Integer> mutate(ArrayList<Integer> parent, Random rnd) {
        ArrayList<Integer> child = new ArrayList<>(parent);
        int n = parent.size();
        int mutations = mutationCount.nextValue();
        for (int i = 0; i < mutations; i++) {
            int index1 = 1 + rnd.nextInt(n - 1);
            int index2 = 1 + rnd.nextInt(n - 1);
            Collections.swap(child, index1, index2);
        }

        return child;
    }
}
