package optimization.mutations;

import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.Probability;

import java.util.ArrayList;
import java.util.Random;

public class HaltonGeneratorCenterInverseMutation extends HaltonGeneratorMutation {
    public HaltonGeneratorCenterInverseMutation(NumberGenerator<Probability> mutationProbability, NumberGenerator<Integer> mutationCount) {
        super(mutationProbability, mutationCount);
    }

    public HaltonGeneratorCenterInverseMutation(Probability mutationProbability) {
        super(mutationProbability);
    }

    @Override
    protected ArrayList<Integer> mutate(ArrayList<Integer> parent, Random rnd) {
        int n = parent.size();
        int index = 1 + rnd.nextInt(n - 1);

        ArrayList<Integer> child = new ArrayList<>();
        child.add(0);

        for (int i = index; i > 0; i--) {
            child.add(parent.get(i));
        }
        for (int i = n - 1; i > index; i--) {
            child.add(parent.get(i));
        }

        return child;
    }
}
