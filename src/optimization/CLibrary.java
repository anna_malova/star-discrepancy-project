package optimization;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.DoubleByReference;

public interface CLibrary extends Library {
    public double oydiscr(Pointer pointset, int dim, int npoints, DoubleByReference lower);
    public double poly_discr(Pointer pointset, int dim, int npoints, DoubleByReference lower);
    public double oldmain(Pointer pointset, int npoints, int dim);
}
