package optimization.crossovers;

import optimization.HaltonGenerator;
import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class HaltonGeneratorCrossover extends AbstractCrossover<HaltonGenerator> {
    protected HaltonGeneratorCrossover(int crossoverPoints) {
        super(crossoverPoints);
    }

    protected abstract ArrayList<ArrayList<Integer>> matePermutations(
            ArrayList<Integer> parent1, ArrayList<Integer> parent2, Random rnd);

    @Override
    protected List<HaltonGenerator> mate(HaltonGenerator parent1, HaltonGenerator parent2, int numberOfCrossoverPoints, Random rng) {
        int dimension = parent1.dimension;

        ArrayList<ArrayList<Integer>> permutations1 = new ArrayList<>();
        ArrayList<ArrayList<Integer>> permutations2 = new ArrayList<>();
        for (int i = 0; i < dimension; i++) {
            ArrayList<ArrayList<Integer>> permutations =
                    matePermutations(parent1.permutations.get(i), parent2.permutations.get(i), rng);
            permutations1.add(permutations.get(0));
            permutations2.add(permutations.get(1));
        }

        ArrayList<HaltonGenerator> children = new ArrayList<>();
        children.add(new HaltonGenerator(dimension, parent1.primes, permutations1));
        children.add(new HaltonGenerator(dimension, parent1.primes, permutations2));

        return children;
    }
}
