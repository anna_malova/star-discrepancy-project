package optimization.crossovers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class HaltonGeneratorCrossoverPMX extends HaltonGeneratorCrossover {
    public HaltonGeneratorCrossoverPMX(int crossoverPoints) {
        super(crossoverPoints);
    }

    private void mapRemainingElements(ArrayList<Integer> child, HashMap<Integer, Integer> hm, int start, int end) {
        for (int i = 1; i < child.size(); i++) {
            if (i >= start && i < end) {
                continue;
            }

            int mapped = child.get(i);
            while (hm.containsKey(mapped))
            {
                mapped = hm.get(mapped);
            }
            child.set(i, mapped);
        }
    }

    @Override
    protected ArrayList<ArrayList<Integer>> matePermutations(ArrayList<Integer> parent1, ArrayList<Integer> parent2, Random rnd) {
        int n = parent1.size();
        int start = 1 + rnd.nextInt(n - 1); int end = 1 + rnd.nextInt(n - 1);
        while (start == end) {
            end = rnd.nextInt(n);
        }
        if (start > end) {
            int tmp = start;
            start = end;
            end = tmp;
        }

        HashMap<Integer, Integer> hm1 = new HashMap<>(n * 3);
        HashMap<Integer, Integer> hm2 = new HashMap<>(n * 3);
        ArrayList<Integer> child1 = new ArrayList<>(parent1);
        ArrayList<Integer> child2 = new ArrayList<>(parent2);

        for (int i = start; i < end; i++)
        {
            child1.set(i, parent2.get(i));
            child2.set(i, parent1.get(i));
            hm1.put(parent2.get(i), parent1.get(i));
            hm2.put(parent1.get(i), parent2.get(i));
        }

        for (int i = 1; i < n; i++) {
            if (i >= start && i < end) {
                continue;
            }

            int mapped = parent1.get(i);
            while (hm1.containsKey(mapped))
            {
                mapped = hm1.get(mapped);
            }

            child1.set(i, mapped);

            mapped = parent2.get(i);
            while (hm2.containsKey(mapped))
            {
                mapped = hm2.get(mapped);
            }

            child2.set(i, mapped);
        }

        ArrayList<ArrayList<Integer>> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);

        // System.err.println("I'm here");
        return children;
    }
}
