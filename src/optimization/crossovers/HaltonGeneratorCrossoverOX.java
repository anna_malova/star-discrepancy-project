package optimization.crossovers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class HaltonGeneratorCrossoverOX extends HaltonGeneratorCrossover {
    public HaltonGeneratorCrossoverOX(int crossoverPoints) {
        super(crossoverPoints);
    }

    @Override
    protected ArrayList<ArrayList<Integer>> matePermutations(ArrayList<Integer> parent1, ArrayList<Integer> parent2, Random rnd) {
        int n = parent1.size();
        int length = 1 + rnd.nextInt(n - 1);
        int start = rnd.nextInt(n - length + 1);
        ArrayList<Integer> child1 = new ArrayList<>(parent1);
        ArrayList<Integer> child2 = new ArrayList<>(parent2);
        HashSet<Integer> used1 = new HashSet<>();
        HashSet<Integer> used2 = new HashSet<>();
        for (int i = start; i < start + length; i++) {
            used1.add(parent1.get(i));
            used2.add(parent2.get(i));
        }

        int idx1 = 1; int idx2 = 1;
        for (int i = 1; i < n; i++) {
            if (i < start || i >= start + length) {
                if (used1.contains(parent2.get(idx1))) {
                    idx1++;
                } else {
                    child1.set(i, parent2.get(idx1));
                }

                if (used2.contains(parent1.get(idx2))) {
                    idx2++;
                } else {
                    child2.set(i, parent1.get(idx2));
                }
            }
        }

        ArrayList<ArrayList<Integer>> children = new ArrayList<>();
        children.add(child1); children.add(child2);
        return children;
    }
}
